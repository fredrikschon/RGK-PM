Detta är ett försök att se hur det skulle fungera att versionshantera medicinska
PM med Markdown och Git. Här ligger Region Kronobergs PM för utredning av misstänkt
Subarachnoidalblödning som ett exempel.

Detta dokument är rekonstruerat utifrån befintlig historik. Det skall dock inte 
ses som en officiell källa. Det formellt giltiga dokumentet, och historiska sådana,
återfinns på Region Kronobergs hemsida.

