Subarachnoidalblödning (misstänkt) / utredning av åskknallshuvudvärk 
=====================================================================

Utarbetat av Fredrik Schön, Medicinkliniken, CLV

Granskat av Peter Kidron, Kim Ekblom, Pär Lindgren

Symtom / Presentation
---------------------

Subarachnoidalblödning (SAB) är ett sjukdomstillstånd som har en
betydande risk för allvarliga komplikationer och död. Det vanligaste
symtomet är hastigt debuterande svår huvudvärk, så kallad
åskknallshuvudvärk, även om enstaka patienter kan debutera med nackvärk,
nackstelhet, ljuskänslighet, illamående/kräkningar, medvetslöshet,
kramper eller stroke-liknande symtom.

Ofta har insjuknandet i manifest SAB föregåtts av huvudvärksattacker av
lindrigare grad, så kallade warnings leaks, och dessa patienter kan
presentera sig med en måttlig huvudvärk utan neurologiska bortfall men
trots detta ha hög risk för förnyad allvarlig blödning. Att identifiera
dessa patienter kan vara livräddande.

Patienter med åskknallshuvudvärk skall i princip utredas med klinisk
undersökning, datortomografi (DT) av hjärnan och lumbalpunktion (LP). LP
är dock inte nödvändig om patienten bedöms ha låg risk för SAB efter
klinisk undersökning och normal DT enligt nedanstående kriterier.

Klinisk undersökning
--------------------

En noggrann anamnes, klinisk undersökning och klinisk riskvärdering
ligger till grund för den fortsatta utredningen. I anamnesen skall
särskilt kartläggas förekomst eller hereditet för aneurysmal kärlsjukdom
eller annan bindvävssjukdom.

|Kliniska kriterier för att kunna utesluta SAB med endast DT
|-----------------------------------------------------------
|• säkerställd symtomdebut
|• avsaknad av andra neurologiska symtom än huvudvärk
|• avsaknad av medvetandeförlust
|• ej isolerad nackvärk
|• normalt neurologiskt status (inklusive ögonbottnar)
|• ej feber eller nackstelhet

Radiologi
---------

Konventionell DT av hjärnan har hög sensitivitet (97%-100%) om den
utförs inom 6 timmar efter symtomdebuten, har optimal teknisk kvalitet
och granskas av radiolog med neuroradiologisk kompetens.
Frågeställningen "Subaraknoidalblödning?" och insjuknandetiden skall
tydligt framgå i röntgenremissen och eventuella tekniska artefakter
skall omnämnas i svaret.

Vid kontraindikation för LP eller inkonklusivt svar kan DT-angiografi
(DTA) användas för skärpt diagnostik. Vid hereditet för aneurysmal SAB
bör DTA göras på vida indikationer.

Likvorprovtagning
-----------------

LP för cellräkning och spektrofotometri bör göras tidigast 12 timmar
efter symtomdebut för att möjliggöra differentiering mellan
stickblödning och egentlig SAB.

LP bör göras med tunnast möjliga nål av icke-skärande typ för att minska
risken för post-punktionell huvudvärk. Lila nål (24G Sprotte) eller
orange nål (25G Sprotte) bör användas.

Prover tas i numrerade plaströr och lämnas omedelbart till laboratoriet
efter provtagningen. Ring laboratoriet innan provtagningen utförs.

|Likvorprov|Analys                              |Provmängd
|----------|------------------------------------|-------------
|1         |Csv-Slaskrör                        | 1-2 ml (\*)
|2         |Csv-Celler , Csv-Protein            | 1-2 ml
|3         |Csv-Spektrofotometri                | 1-2 ml
|4         |Csv-Erc, extra                      | 1-2 ml
|          |\+ eventuella ytterligare analyser  |
|          |                                    |
|Blodprov  |S-Bilirubin, S-Protein              | Gult Serumrör

(\*) Helst till makroskopiskt klar likvor ses.  

Vidare handläggning av påvisad SAB
----------------------------------

Verifierad SAB skall utredas akut för bakomliggande aneurysm i samråd
med neurokirurg (046-177 354). DT och DTA utförs och demonstreras för
neurokirurgen. I flertalet fall överförs patienten till Lund för
konventionell angiografi, eventuellt med endovaskulär eller kirurgisk
åtgärd. Blodtrycksbehandling (inj Trandate 5mg/ml 5-20 ml iv) och
eventuellt antifibrinolytisk behandling (inj Cyklokapron 100mg/ml 10ml
iv) kan bli aktuellt inför transport. Samråd med anestesijouren om
anestesipersonal skall följa med. Om patienten är medvetandesänkt bör
intubation övervägas inför transporten.

Vidare handläggning vid utesluten SAB
-------------------------------------

Om SAB har uteslutits efter ovanstående utredning har patienten mycket
låg risk att återinsjukna i hjärnblödning. Innan huvudvärken avskrivs
som godartad måste dock relevanta differentialdiagnoser övervägas,
exempelvis stroke, arteriella dissektioner, temporalisarterit,
CNS-infektioner, sinustrombos och hypofysapoplexi.

Referenser
----------

[Nationella Riktlinjer för Strokesjukvård; Socialstyrelsen](http://www.socialstyrelsen.se/nationellariktlinjerforstrokesjukvard)

[AHA / ASA Guidelines for the Management of Aneurysmal Subarachnoid
Hemorrhage; Stroke. 2012;43:1711-1737](http://stroke.ahajournals.org/content/43/6/1711.long)

[Cruickshank A et al; Revised national guidelines for analysis of
cerebrospinal fluid for bilirubin in suspected subarachnoid haemorrhage;
Ann Clin Biochem 2008;45:238--244](http://journals.sagepub.com/doi/full/10.1258/acb.2008.007257)

[Dubosh N M et al; Sensitivity of Early Brain Computed Tomography to
Exclude Aneurysmal Subarachnoid Hemorrhage - A Systematic Review and
Meta-Analysis; Stroke. 2016;47:750-755](http://stroke.ahajournals.org/content/47/3/750.long)

